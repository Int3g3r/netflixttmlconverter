﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TTMLtoSRT
{
  class Convert
  {
    private List<string> content = new List<string>();
    private string beginToEndStr = " --> ";
    private string toConvert;
    private Int32 tickRate;
    private Int32 srtCount = 1;

    public void convertLine(string _string)
    {
      toConvert = _string;

      if (toConvert.Contains("<p begin="))
      {
        StringBuilder s = new StringBuilder(srtCount.ToString());
        s.AppendLine();
        string beginTime = convertTime("begin=\"", "t\"");
        string endTime = convertTime("end=\"", "t\"");
        s.Append(beginTime);
        s.Append(beginToEndStr);
        s.Append(endTime);
        s.AppendLine();
        s.Append(getTranslation("\">"));
        content.Add(s.ToString());
        srtCount++;
      }
    }

    public List<string> getContent()
    {
      return content;
    }

    private string convertTime(string _searchBeginStr, string _searchEndStr)
    {
      Int32 beginPos = 0;
      Int32 endPos = 0;

      beginPos = toConvert.IndexOf(_searchBeginStr) + _searchBeginStr.Length;
      endPos = toConvert.IndexOf(_searchEndStr, beginPos);
      string tmpTimeStr = toConvert.Substring(beginPos, endPos - beginPos);
      double tmpTimeFloat = System.Convert.ToDouble(tmpTimeStr) / tickRate;
      return TimeSpan.FromSeconds(tmpTimeFloat).ToString("hh':'mm':'ss','fff");
    }

    private string getTranslation(string _endSearch)
    {
      string tmpLine = toConvert;

      Int32 endPos = tmpLine.IndexOf(_endSearch) + _endSearch.Length;
      tmpLine = tmpLine.Remove(0, endPos);
      tmpLine = tmpLine.Replace("</p>", "");
      tmpLine = tmpLine.Replace("</span>", "");
      tmpLine = tmpLine.Replace("<br/>", System.Environment.NewLine);

      while(tmpLine.Contains("<span"))
      {
        Int32 beginPos = tmpLine.IndexOf("<span");
        endPos = tmpLine.IndexOf("\">", beginPos + "<span".Length) + "\">".Length;
        tmpLine = tmpLine.Remove(beginPos, endPos - beginPos);
      }
      tmpLine = tmpLine + System.Environment.NewLine;

      return tmpLine;
    }

    public Int32 saveToFile(string _fileName)
    {

      SaveFileDialog save = new SaveFileDialog();
      save.Title = "Save File";
      save.FileName = Path.GetFileNameWithoutExtension(_fileName);
      save.Filter = "srt (*.srt)|*.srt|All files (*.*)|*.*";
      save.FilterIndex = 1;
      if (save.ShowDialog() == DialogResult.OK)
      {
        System.IO.File.WriteAllLines(save.FileName, content);
        return 1;
      }
      return 0;
    }

    public Int32 saveMultipleFiles(string _fileName)
    {
      System.IO.File.WriteAllLines(_fileName, content);
      return 1;
    }


    public Convert(Int32 _tickRate)
    {
      tickRate = _tickRate;
    }
  }
}
