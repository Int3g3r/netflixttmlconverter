﻿namespace TTMLtoSRT
{
  partial class frmMain
  {
    /// <summary>
    /// Erforderliche Designervariable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Verwendete Ressourcen bereinigen.
    /// </summary>
    /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Vom Windows Form-Designer generierter Code

    /// <summary>
    /// Erforderliche Methode für die Designerunterstützung.
    /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
      this.btnOpen = new System.Windows.Forms.Button();
      this.btnSave = new System.Windows.Forms.Button();
      this.tabControl1 = new System.Windows.Forms.TabControl();
      this.tabPage1 = new System.Windows.Forms.TabPage();
      this.original = new System.Windows.Forms.ListBox();
      this.tabPage2 = new System.Windows.Forms.TabPage();
      this.txtConverted = new System.Windows.Forms.TextBox();
      this.btnConvert = new System.Windows.Forms.Button();
      this.tickRate = new System.Windows.Forms.TextBox();
      this.btnClose = new System.Windows.Forms.Button();
      this.debug = new System.Windows.Forms.Label();
      this.lblStatus = new System.Windows.Forms.Label();
      this.lblInstructions = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.radioBox = new System.Windows.Forms.GroupBox();
      this.radioNone = new System.Windows.Forms.RadioButton();
      this.radioDelete = new System.Windows.Forms.RadioButton();
      this.radioMove = new System.Windows.Forms.RadioButton();
      this.checkFolder = new System.Windows.Forms.CheckBox();
      this.tabControl1.SuspendLayout();
      this.tabPage1.SuspendLayout();
      this.tabPage2.SuspendLayout();
      this.radioBox.SuspendLayout();
      this.SuspendLayout();
      // 
      // btnOpen
      // 
      this.btnOpen.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnOpen.Location = new System.Drawing.Point(10, 24);
      this.btnOpen.Name = "btnOpen";
      this.btnOpen.Size = new System.Drawing.Size(100, 30);
      this.btnOpen.TabIndex = 1;
      this.btnOpen.Text = "File...";
      this.btnOpen.UseVisualStyleBackColor = true;
      this.btnOpen.Click += new System.EventHandler(this.BtnOpen_Click);
      // 
      // btnSave
      // 
      this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnSave.Location = new System.Drawing.Point(256, 24);
      this.btnSave.Name = "btnSave";
      this.btnSave.Size = new System.Drawing.Size(100, 30);
      this.btnSave.TabIndex = 2;
      this.btnSave.Text = "Save";
      this.btnSave.UseVisualStyleBackColor = true;
      this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
      // 
      // tabControl1
      // 
      this.tabControl1.Controls.Add(this.tabPage1);
      this.tabControl1.Controls.Add(this.tabPage2);
      this.tabControl1.Location = new System.Drawing.Point(12, 115);
      this.tabControl1.Name = "tabControl1";
      this.tabControl1.SelectedIndex = 0;
      this.tabControl1.Size = new System.Drawing.Size(1153, 627);
      this.tabControl1.TabIndex = 3;
      // 
      // tabPage1
      // 
      this.tabPage1.Controls.Add(this.original);
      this.tabPage1.Location = new System.Drawing.Point(4, 22);
      this.tabPage1.Name = "tabPage1";
      this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage1.Size = new System.Drawing.Size(1145, 601);
      this.tabPage1.TabIndex = 0;
      this.tabPage1.Text = "Original";
      this.tabPage1.UseVisualStyleBackColor = true;
      // 
      // original
      // 
      this.original.Dock = System.Windows.Forms.DockStyle.Fill;
      this.original.FormattingEnabled = true;
      this.original.Location = new System.Drawing.Point(3, 3);
      this.original.Name = "original";
      this.original.SelectionMode = System.Windows.Forms.SelectionMode.None;
      this.original.Size = new System.Drawing.Size(1139, 595);
      this.original.TabIndex = 0;
      // 
      // tabPage2
      // 
      this.tabPage2.Controls.Add(this.txtConverted);
      this.tabPage2.Location = new System.Drawing.Point(4, 22);
      this.tabPage2.Name = "tabPage2";
      this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage2.Size = new System.Drawing.Size(1145, 601);
      this.tabPage2.TabIndex = 1;
      this.tabPage2.Text = "Converted";
      this.tabPage2.UseVisualStyleBackColor = true;
      // 
      // txtConverted
      // 
      this.txtConverted.BackColor = System.Drawing.SystemColors.ButtonHighlight;
      this.txtConverted.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtConverted.Location = new System.Drawing.Point(3, 3);
      this.txtConverted.Multiline = true;
      this.txtConverted.Name = "txtConverted";
      this.txtConverted.ReadOnly = true;
      this.txtConverted.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txtConverted.Size = new System.Drawing.Size(1139, 595);
      this.txtConverted.TabIndex = 2;
      this.txtConverted.Text = "sdffsdfsdf";
      // 
      // btnConvert
      // 
      this.btnConvert.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnConvert.Location = new System.Drawing.Point(134, 24);
      this.btnConvert.Name = "btnConvert";
      this.btnConvert.Size = new System.Drawing.Size(100, 30);
      this.btnConvert.TabIndex = 4;
      this.btnConvert.Text = "Convert";
      this.btnConvert.UseVisualStyleBackColor = true;
      this.btnConvert.Click += new System.EventHandler(this.BtnConvert_Click);
      // 
      // tickRate
      // 
      this.tickRate.Location = new System.Drawing.Point(528, 67);
      this.tickRate.Name = "tickRate";
      this.tickRate.ReadOnly = true;
      this.tickRate.Size = new System.Drawing.Size(100, 20);
      this.tickRate.TabIndex = 5;
      // 
      // btnClose
      // 
      this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnClose.Location = new System.Drawing.Point(528, 22);
      this.btnClose.Name = "btnClose";
      this.btnClose.Size = new System.Drawing.Size(100, 30);
      this.btnClose.TabIndex = 6;
      this.btnClose.Text = "Close";
      this.btnClose.UseVisualStyleBackColor = true;
      this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
      // 
      // debug
      // 
      this.debug.AutoSize = true;
      this.debug.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.debug.Location = new System.Drawing.Point(436, 63);
      this.debug.Name = "debug";
      this.debug.Size = new System.Drawing.Size(86, 25);
      this.debug.TabIndex = 7;
      this.debug.Text = "DEBUG";
      this.debug.Visible = false;
      // 
      // lblStatus
      // 
      this.lblStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblStatus.ForeColor = System.Drawing.Color.MediumBlue;
      this.lblStatus.Location = new System.Drawing.Point(0, 95);
      this.lblStatus.Name = "lblStatus";
      this.lblStatus.Size = new System.Drawing.Size(647, 23);
      this.lblStatus.TabIndex = 8;
      this.lblStatus.Text = "Status ...";
      this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lblInstructions
      // 
      this.lblInstructions.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblInstructions.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblInstructions.ForeColor = System.Drawing.Color.MediumBlue;
      this.lblInstructions.Location = new System.Drawing.Point(0, 0);
      this.lblInstructions.Name = "lblInstructions";
      this.lblInstructions.Size = new System.Drawing.Size(647, 19);
      this.lblInstructions.TabIndex = 9;
      this.lblInstructions.Text = "Open a Netflix-TTML File";
      this.lblInstructions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // label1
      // 
      this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.ForeColor = System.Drawing.Color.Black;
      this.label1.Location = new System.Drawing.Point(546, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(101, 17);
      this.label1.TabIndex = 10;
      this.label1.Text = "created by Int3g3r";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // radioBox
      // 
      this.radioBox.Controls.Add(this.radioNone);
      this.radioBox.Controls.Add(this.radioDelete);
      this.radioBox.Controls.Add(this.radioMove);
      this.radioBox.Location = new System.Drawing.Point(10, 60);
      this.radioBox.Name = "radioBox";
      this.radioBox.Size = new System.Drawing.Size(420, 30);
      this.radioBox.TabIndex = 11;
      this.radioBox.TabStop = false;
      // 
      // radioNone
      // 
      this.radioNone.AutoSize = true;
      this.radioNone.Location = new System.Drawing.Point(2, 10);
      this.radioNone.Name = "radioNone";
      this.radioNone.Size = new System.Drawing.Size(51, 17);
      this.radioNone.TabIndex = 2;
      this.radioNone.TabStop = true;
      this.radioNone.Text = "None";
      this.radioNone.UseVisualStyleBackColor = true;
      // 
      // radioDelete
      // 
      this.radioDelete.AutoSize = true;
      this.radioDelete.Location = new System.Drawing.Point(291, 8);
      this.radioDelete.Name = "radioDelete";
      this.radioDelete.Size = new System.Drawing.Size(111, 17);
      this.radioDelete.TabIndex = 1;
      this.radioDelete.TabStop = true;
      this.radioDelete.Text = "Delete original File";
      this.radioDelete.UseVisualStyleBackColor = true;
      // 
      // radioMove
      // 
      this.radioMove.AutoSize = true;
      this.radioMove.Location = new System.Drawing.Point(96, 10);
      this.radioMove.Name = "radioMove";
      this.radioMove.Size = new System.Drawing.Size(154, 17);
      this.radioMove.TabIndex = 0;
      this.radioMove.TabStop = true;
      this.radioMove.Text = "move in \\backupSub folder";
      this.radioMove.UseVisualStyleBackColor = true;
      // 
      // checkFolder
      // 
      this.checkFolder.AutoSize = true;
      this.checkFolder.Location = new System.Drawing.Point(374, 31);
      this.checkFolder.Name = "checkFolder";
      this.checkFolder.Size = new System.Drawing.Size(86, 17);
      this.checkFolder.TabIndex = 12;
      this.checkFolder.Text = "Multiple Files";
      this.checkFolder.UseVisualStyleBackColor = true;
      this.checkFolder.CheckedChanged += new System.EventHandler(this.CheckFolder_CheckedChanged);
      // 
      // frmMain
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(647, 118);
      this.Controls.Add(this.checkFolder);
      this.Controls.Add(this.radioBox);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.lblInstructions);
      this.Controls.Add(this.lblStatus);
      this.Controls.Add(this.debug);
      this.Controls.Add(this.btnClose);
      this.Controls.Add(this.tickRate);
      this.Controls.Add(this.btnConvert);
      this.Controls.Add(this.tabControl1);
      this.Controls.Add(this.btnSave);
      this.Controls.Add(this.btnOpen);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.Name = "frmMain";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "TTML to SRT Netflix Converter";
      this.Load += new System.EventHandler(this.FrmMain_Load);
      this.tabControl1.ResumeLayout(false);
      this.tabPage1.ResumeLayout(false);
      this.tabPage2.ResumeLayout(false);
      this.tabPage2.PerformLayout();
      this.radioBox.ResumeLayout(false);
      this.radioBox.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion
    private System.Windows.Forms.Button btnOpen;
    private System.Windows.Forms.Button btnSave;
    private System.Windows.Forms.TabControl tabControl1;
    private System.Windows.Forms.TabPage tabPage1;
    private System.Windows.Forms.ListBox original;
    private System.Windows.Forms.TabPage tabPage2;
    private System.Windows.Forms.Button btnConvert;
    private System.Windows.Forms.TextBox tickRate;
    private System.Windows.Forms.TextBox txtConverted;
    private System.Windows.Forms.Button btnClose;
    private System.Windows.Forms.Label debug;
    private System.Windows.Forms.Label lblStatus;
    private System.Windows.Forms.Label lblInstructions;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.GroupBox radioBox;
    private System.Windows.Forms.RadioButton radioDelete;
    private System.Windows.Forms.RadioButton radioMove;
    private System.Windows.Forms.RadioButton radioNone;
    private System.Windows.Forms.CheckBox checkFolder;
  }
}

