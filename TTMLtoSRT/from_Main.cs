﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TTMLtoSRT
{
  public partial class frmMain : Form
  {
    private string backupSubFolder = "backupSub";
    public string tickRateStr;
    private Convert con;
    private List<string> origData = new List<string>();
    
    private string originalFilePath; // fileName + Path
    private string originalFileName; // nameOnly
    private string originalFilePathOnly; // PathOnly

    private struct struBntControl
    {
      public bool FileOpened;
      public bool FileConverted;
    }

    struBntControl btnControl = new struBntControl();

    private List<string> multiFilePath = new List<string>();
    private List<string> multiFileName = new List<string>();
    private List<string> multiFilePathOnly = new List<string>();

    public frmMain()
    {
      InitializeComponent();
    }

    private void BtnClose_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void BtnOpen_Click(object sender, EventArgs e)
    {
      if (checkFolder.Checked)
      {
        openFolder();
      }
      else
      {
        openFile();
      }
    }

    private void BtnConvert_Click(object sender, EventArgs e)
    { 
      if (checkFolder.Checked)
      {
        convertFolder();
      }
      else
      {
        convertFile();
      }
    }

    private void searchTickRate()
    {
      for (Int32 i = 0; i < origData.Count - 1; i++)
      {
        string strSearchBegin = "tickRate=\"";
        string strSearchEnd = "\"";
        Int32 beginPos = 0;
        Int32 endPos = 0;

        string search = origData[i].ToString();

        beginPos = search.IndexOf(strSearchBegin);
        if (beginPos != -1 && beginPos != 0)
        {
          endPos = search.IndexOf(strSearchEnd, beginPos + strSearchBegin.Length);
        }

        if (endPos != 0)
        {
          tickRateStr = search.Substring(beginPos + strSearchBegin.Length, endPos - beginPos - strSearchBegin.Length);
          if (tickRate.Visible)
          {
            tickRate.Text = tickRateStr;
          }
          break;
        }
      }
    }

    private void BtnSave_Click(object sender, EventArgs e)
    {
      
      if (con.saveToFile(originalFileName) == 1)
      {
        string tmpOrigFileName = originalFilePathOnly + "\\" + originalFileName;
       
        if (radioMove.Checked)
        {
          string destFile = originalFilePathOnly + "\\" + backupSubFolder + "\\" + originalFileName;

          if (!(Directory.Exists(originalFilePathOnly + "\\" + backupSubFolder + "\\")))
          {
            Directory.CreateDirectory(originalFilePathOnly + "\\" + backupSubFolder + "\\");
          }

          File.Move(tmpOrigFileName, destFile);

        }
        if (radioDelete.Checked)
        {
          File.Delete(tmpOrigFileName);
        }




        //if (radioMove.Checked)
        //{
        //  string destFile = originalFileWithPath;
        //  destFile = destFile.Replace(originalFileNameWithExtension, originalFileName + ".backup");
        //  File.Move(originalFileWithPath, destFile);
        //}
        //if (radioDelete.Checked)
        //{
        //  File.Delete(originalFileWithPath);
        //}


        lblStatus.Text = "Save done! Enjoy.";
        lblInstructions.Text = "Open a Netflix-TTML File";
        btnConvert.Enabled = false;
        btnSave.Enabled = false;
      }
    }

    private void FrmMain_Load(object sender, EventArgs e)
    {
      btnConvert.Enabled = false;
      btnSave.Enabled = false;
      radioNone.Checked = true;

      if (debug.Visible)
      {
        tabControl1.Enabled = true;
        tabControl1.SelectedTab = tabPage1;
        tickRate.Visible = true;
        FormBorderStyle = FormBorderStyle.Sizable;
      }
      else
      {
        tabControl1.Enabled = false;
        tabControl1.Visible = false;
        tickRate.Visible = false;
        FormBorderStyle = FormBorderStyle.FixedSingle;
      }
    }

    private void CheckFolder_CheckedChanged(object sender, EventArgs e)
    {
      if (checkFolder.Checked)
      {
        btnOpen.Text = "Folder...";
        lblInstructions.Text = "Select new Folder..";
        btnConvert.Enabled = false;
        btnSave.Enabled = false;
      }
      else
      {
        btnOpen.Text = "File...";
        lblInstructions.Text = "Open a Netflix-TTML File";
        btnConvert.Enabled = false;
        btnSave.Enabled = false;
      }
    }

    private void openFile()
    {
      using (OpenFileDialog openFileDialog = new OpenFileDialog())
      {
        openFileDialog.Filter = "TTML File (*.ttml)|*.ttml|All files (*.*)|*.*";
        openFileDialog.FilterIndex = 1;
        openFileDialog.RestoreDirectory = false;

        if (openFileDialog.ShowDialog() == DialogResult.OK)
        {
          originalFileName = Path.GetFileName(openFileDialog.FileName);
          originalFilePathOnly = Path.GetDirectoryName(openFileDialog.FileName);
          originalFilePath = openFileDialog.FileName;

          origData.Clear();
          if (debug.Visible)
          {
            original.Items.Clear();
          }

          System.IO.StreamReader file = new System.IO.StreamReader(originalFilePath);
          int counter = 0;
          string line;
          while ((line = file.ReadLine()) != null)
          {
            origData.Add(line);

            if (debug.Visible)
            {
              original.Items.Add(line);
            }

            counter++;
          }

          if (debug.Visible)
          {
            txtConverted.Clear();
            tabControl1.SelectedTab = tabPage1;
          }
          lblStatus.Text = "File openig sucessful !";
          lblInstructions.Text = "Now you can press convert.";
          btnConvert.Enabled = true;
          file.Close();
          btnControl.FileOpened = true;
        }
      }
    }

    private void convertFile()
    {
      tickRateStr = null;
      tickRate.Text = "";
      if (debug.Visible)
      {
        txtConverted.Clear();
      }
      searchTickRate();
      if (tickRateStr == "" || tickRateStr == null)
      {
        MessageBox.Show("ERROR: trickRate not found. Abort!");
        btnConvert.Enabled = false;
        return;
      }

      if (tickRateStr != "")
      {
        con = new Convert(System.Convert.ToInt32(tickRateStr));

        for (Int32 i = 0; i < origData.Count - 1; i++)
        {
          con.convertLine(origData[i].ToString());
        }
        lblStatus.Text = "Conversion done.";
        lblInstructions.Text = "Now you can save the convertet TTML File";
        btnSave.Enabled = true;
        btnControl.FileConverted = true;
      }

      if (debug.Visible)
      {
        foreach (string line in con.getContent())
        {
          txtConverted.AppendText(line);
          txtConverted.AppendText(System.Environment.NewLine);
        }
        tabControl1.SelectedTab = tabPage2;
      }
    }
  
    private void openFolder()
    {
      using (FolderBrowserDialog folderDlg = new FolderBrowserDialog())
      {
        if (folderDlg.ShowDialog() == DialogResult.OK)
        {
          string[] tmpFileNames = Directory.GetFiles(folderDlg.SelectedPath,"*.ttml", SearchOption.AllDirectories);
          multiFilePath = tmpFileNames.ToList();

          foreach (string element in multiFilePath)
          {
            multiFileName.Add(Path.GetFileNameWithoutExtension(element));
            multiFilePathOnly.Add(Path.GetDirectoryName(element));
          }

          btnConvert.Enabled = true;
          lblStatus.Text = "File openig sucessful !";
          lblInstructions.Text = "Now you can press convert.";
        }
      }
    }
    
    private void convertFolder()
    {
      if (multiFilePath.Count > 0)
      {
        for (int i = 0; i < multiFilePath.Count; i++)
        {
          if (debug.Visible)
          {
            original.Items.Clear();
          }

          System.IO.StreamReader file = new System.IO.StreamReader(multiFilePath[i]);
          int counter = 0;
          string line;
          while ((line = file.ReadLine()) != null)
          {
            origData.Add(line);

            if (debug.Visible)
            {
              original.Items.Add(line);
            }

            counter++;
          }

          if (debug.Visible)
          {
            txtConverted.Clear();
            tabControl1.SelectedTab = tabPage1;
          }
          file.Close();

          //READY FOR CONVERSION ... search Tickrate

          tickRateStr = null;
          tickRate.Text = "";
          if (debug.Visible)
          {
            txtConverted.Clear();
          }
          searchTickRate();
          if (tickRateStr == "" || tickRateStr == null)
          {
            MessageBox.Show("ERROR: trickRate not found. Abort!");
            btnConvert.Enabled = false;
            return;
          }

          if (tickRateStr != "")
          {
            con = new Convert(System.Convert.ToInt32(tickRateStr));

            for (Int32 x = 0; x < origData.Count - 1; x++)
            {
              con.convertLine(origData[x].ToString());
            }
            //Ready to save ... save now
            string tmpNewFileName = multiFilePathOnly[i] + "\\" + multiFileName[i] + ".srt";
            con.saveMultipleFiles(tmpNewFileName);


            if (radioMove.Checked)
            {
              string destFile = multiFilePathOnly[i] + "\\" + backupSubFolder + "\\" + multiFileName[i] + ".ttml";

              if (!(Directory.Exists(multiFilePathOnly[i] + "\\" + backupSubFolder + "\\")))
              {
                Directory.CreateDirectory(multiFilePathOnly[i] + "\\" + backupSubFolder + "\\");
              }

              File.Move(multiFilePath[i], destFile);
            }
            if (radioDelete.Checked)
            {
              File.Delete(multiFilePath[i]);
            }
            origData.Clear();

            lblStatus.Text = "Save done! Enjoy.";
            lblInstructions.Text = "Select new Folder..";
            btnConvert.Enabled = false;
            btnSave.Enabled = false;
          }

          if (debug.Visible)
          {
            foreach (string lineB in con.getContent())
            {
              txtConverted.AppendText(lineB);
              txtConverted.AppendText(System.Environment.NewLine);
            }
            tabControl1.SelectedTab = tabPage2;
          }

        }
      }
      else
      {
        MessageBox.Show("ERROR: 0 .ttml files found! Abort!");
      }
    }
  }
}
